import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Redux
import { connect } from 'react-redux';
import { loadCard } from '../../redux/cart-redux';
// App
import Header from '../header';
import CartEmpty from '../cart-empty';
import CartShop from '../cart-shop';


class Cart extends Component {
  constructor() {
    super();
    this.state = {
      test: true
    };
  }

  componentDidMount() {
    this.props.loadCard();
  }

  render() {
    // console.log('==== props', this.props);

    return (
      <div>
        <Header />
        {this.props.cart.payload && this ? <CartShop /> : <CartEmpty />}
      </div>
    );
  }
}


// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
Cart.propTypes = {
  cart: PropTypes.shape({
    payload: PropTypes.shape()
  }).isRequired,
  loadCard: PropTypes.func.isRequired
};

// Redux ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const mapStateToProps = ({ cart }) => ({ cart });
export default connect(mapStateToProps, { loadCard })(Cart);
