import React from 'react';
// import PropTypes from 'prop-types';


// Component ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function Header() {
  return (
    <header id="header" className="header">
      <div className="header-wrapper">
        <h1 className="header-title">Walmart.com</h1>
      </div>
    </header>
  );
}

// Settings :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Header.propTypes = {
//   data: PropTypes.shape({}).isRequired
// };

export default Header;
