import React from 'react';
import { render } from 'react-dom';
// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
// App
import reducers from './redux/reducers';
import Cart from './components/cart';


const store = createStore(reducers, applyMiddleware(thunk));
const cartApp = (
  <Provider store={store}>
    <Cart />
  </Provider>
);

render(cartApp, document.querySelector('#cart-app'));
