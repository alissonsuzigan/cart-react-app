// Actions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const LOADING = 'LOADING';
const LOAD_CARD = 'LOAD_CARD';
const LOAD_CARD_ERROR = 'LOAD_CARD_ERROR';
// const UPDATE_CARD = 'UPDATE_CARD';
// const CHANGE_ITEM_QUANTITY = 'CHANGE_ITEM_QUANTITY';
// const REMOVE_ITEM =     'REMOVE_ITEM';
// const ESTIMATE_DELIVERY = 'ESTIMATE_DELIVERY';
// const ADD_GIFT_CARD = 'ADD_GIFT_CARD';


// Action Creators ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const cookies = {
  credentials: 'same-origin'
};

// Load Cart
export function loadCard() {
  const url = `${app.config.domain}${app.config.services.loadCart.url}`;


  return (dispatch) => {
    dispatch({ type: LOADING });

    fetch(url, cookies)
      .then(response => (response.ok ? response.json() : response.json().then(Promise.reject.bind(Promise))))
      .then(result => dispatch({
        type: LOAD_CARD,
        payload: result
      }))
      .catch(error => dispatch({
        type: LOAD_CARD_ERROR,
        payload: error
      }));
  };
}


// Reducer ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const DEFAULT_STATE = {
  error: false,
  loading: false
};
const DEFAULT_ACTION = {
  type: LOADING
};

export default function reducer(state = DEFAULT_STATE, action = DEFAULT_ACTION) {
  switch (action.type) {
    case LOADING:
      return { ...state, ...DEFAULT_STATE, loading: true };

    case LOAD_CARD:
      return { ...state, ...DEFAULT_STATE, payload: action.payload };

    case LOAD_CARD_ERROR:
      return { ...state, ...DEFAULT_STATE, payload: action.payload, error: true };

    default:
      return state;
  }
}
