import { combineReducers } from 'redux';
import Cart from './cart-redux';


// Combine Reducers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
const reducers = combineReducers({
  cart: Cart
});

export default reducers;
