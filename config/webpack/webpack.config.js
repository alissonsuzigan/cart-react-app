const path = require('path');
// Global paths
const buildPath = path.resolve(process.cwd(), 'build');
const configPath = path.resolve(process.cwd(), 'config');
const sourcePath = path.resolve(process.cwd(), 'src');

// Loaders and plugins
const autoprefixer = require('autoprefixer');
const ExtractCSS = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const StyleLint = require('stylelint-webpack-plugin');
const webpack = require('webpack');

// Local configs
const appVersion = require('../../package.json').version;
const appConfig = require('../env/dev.json');


// Webpack configs ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
module.exports = {
  context: sourcePath,
  devtool: 'eval',

  entry: {
    app: ['index.jsx'],
    vendor: [
      'react',
      'react-dom'
    ]
  },

  output: {
    filename: 'static/js/[chunkhash:8].[name].js',
    path: buildPath
  },

  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
    alias: {
      scripts: `${sourcePath}/assets/scripts`,
      styles: `${sourcePath}/assets/styles`,
      images: `${sourcePath}/assets/images`
    },
    modules: [
      'node_modules',
      sourcePath
    ]
  },


  // dev-server configs :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  devServer: {
    contentBase: sourcePath,
    compress: true,
    port: 8888,
    // open: true,
    stats: {
      assets: true,
      children: false,
      modules: false
    },
    headers: {
      'X-WM-Token': 'yes',
    },
    proxy: {
      '/services/*': {
        target: 'https://qa.checkout.waldev.com.br/checkout',
        changeOrigin: true
        // secure: false
      }
    }
  },


  // Modules ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  module: {
    rules: [
      { // eslint config
        test: /\.jsx?$/,
        enforce: 'pre',
        include: sourcePath,
        loader: 'eslint-loader',
        options: {
          configFile: `${configPath}/.eslintrc`
          // fix: true // autofix eslint errors
        }
      },

      { // babel config
        test: /\.jsx?$/,
        include: sourcePath,
        use: [
          'babel-loader'
        ]
      },

      { // scss config
        test: /\.scss$/,
        include: sourcePath,
        use: ExtractCSS.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [autoprefixer]
              }
            },
            'sass-loader'
          ]
        })
      }

    ]
  },


  // Plugins ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  plugins: [
    // Extract CSS
    new ExtractCSS('static/css/[contenthash:8].[name].css'),

    // Generate HTML file
    new HtmlPlugin({
      template: 'index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      }
    }),

    // Lint scss files
    new StyleLint({
      configFile: `${configPath}/.stylelintrc/`,
      context: sourcePath,
      syntax: 'scss'
    }),

    // Build separeted vendor file
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity
    }),


    new webpack.DefinePlugin({
      app: {
        config: JSON.stringify(appConfig),
        env: JSON.stringify('dev'),
        version: JSON.stringify(appVersion)
      }
    })

    // Minify scripts
    // new webpack.optimize.UglifyJsPlugin(),
  ]
};
